package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        boolean res = true;

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        if (x.size() > y.size()) {
            res = false;
        } else {
            Iterator<Object> iterX = x.iterator();

            while (iterX.hasNext()) {
                Iterator<Object> iterY = y.iterator();
                Object strX = iterX.next();

                while (iterY.hasNext()) {
                    Object strY = iterY.next();
                    iterY.remove();
                    if (!strX.equals(strY)) {
                        res = false;
                    } else {
                        res = true;
                        break;
                    }
                }
            }


        }
        return res;
    }
}
