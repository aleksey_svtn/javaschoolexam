package com.tsystems.javaschool.tasks.calculator;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");
        String res = null;
        try {
            statement = Calculator.validateExpression(statement);
            String eval_res = engine.eval(statement).toString();
            if (Calculator.isNumeric(eval_res)) {
                res = eval_res;
            }
        } catch (ScriptException e) {
            System.out.println("Log:problems in JS eval():");
        } catch (Exception e) {
            System.out.println("Log:problems in expression:" + e.getMessage());
        }

        return res;
    }

    private static String validateExpression(String statement) {
        Pattern pattern = Pattern.compile("([-.+\\/*]?[(]*?[0-9]+[)]*)*");
        if (!pattern.matcher(statement).matches()) {
            throw new IllegalArgumentException("Invalid expression");
        }
        return statement;
    }

    private static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
            if (Double.isInfinite(d)) {
                return false;
            }
        } catch (NumberFormatException nfe) {
            return false;
        }

        return true;
    }

}
