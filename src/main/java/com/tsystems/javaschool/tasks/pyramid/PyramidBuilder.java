package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int[][] matrix;//Получившаяся матрица

        try {
            // проверка на возможность построения пирамиды взята со stackoverflow  А.С.Савайтан
            int size = inputNumbers.size();//Проверяем размер полученного массива

            //Проверим, является ли данное число треугольным
            int count = 0;
            int rows = 1;
            int cols = 1;
            while (count < size) {
                count = count + rows;
                rows++;
                cols = cols + 2;
            }

            rows = rows - 1;//Актуальное число строк
            cols = cols - 2;//Актуальное число столбцов

            if (size != count) {
                throw new Exception();
            }

            Collections.sort(inputNumbers);

            matrix = new int[rows][cols];//Задаем размерность матрице

            int centr = (cols / 2);//Находим центральную точку матрицы
            int sort_index = 0;
            for (int i = 0; i < matrix.length; i++) {
                int numbers_on_row = i + 1;
                for (int j = 0; j < matrix[0].length; j++) {
                    int start_pos = centr - i;
                    int end_pos = centr + i;
                    if (j < start_pos || inputNumbers.size() <= sort_index || (j > end_pos) || (start_pos - 1 + j) % 2 == 0 && i != 0) {
                        matrix[i][j] = 0;
                    } else {
                        matrix[i][j] = inputNumbers.get(sort_index);
                        sort_index++;
                    }
                }
            }
            /* Вывод закоментили
            for(int [] a: matrix)//выводим матрицу на экран
            {
                for(int b: a)
                    System.out.print(b+" ");
                System.out.println();
            }
            */
        } catch (Throwable e) {
            //todo оптимизировать
            throw new CannotBuildPyramidException();
        }
        return matrix;
    }

}
